// Uplift, Inc. Presentation Resource Loader

var pagesToOpen = [
    "https://html5-michaelpeng2002.c9users.io/upliftinc.org/web/index.html",
    "https://gitlab.com/broadwall/upliftinc.org/blob/master/workspace/css/custom.css",
    "https://gitlab.com/broadwall/upliftinc.org/blob/master/workspace/babel/custom.js",
    "https://gitlab.com/broadwall/upliftinc.org/blob/master/workspace/work-history.md",
    "http://upliftinc.org"
];

for (var i = 1; i < pagesToOpen.length; ++i) {
    window.open(pagesToOpen[i]);
}

window.location.href = pagesToOpen[0];

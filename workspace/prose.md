Prose for Uplift, Inc. Website
===

_Purpose: Present information and goal of organization and provide opportunities to donate_
_Communication: Use short paragraphs and blocks often; avoid long paragraphs and "essays"_
_Typeface: Roboto / Lato / (consult Canva)_
_Colors: Material Design Palette_
_Images: (ask for it)_
_Navigation: Top navbar, bottom links_
_Grid: Elements_
_F layout_
_Responsiveness: adjust to screen widths
_Minimize load time_

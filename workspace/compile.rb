#!/usr/bin/env ruby

Targets = {
    "custom.js" => [
        "java -jar compiler/closure-compiler-v20161024.jar --js babel/custom.js \
--js_output_file ../web/js/custom.js"
    ],
    "custom.css" => [
        "java -jar compiler/yuicompressor-2.4.8.jar css/custom.css -o \
../web/css/custom.css"
    ]
}

# ----------
# Fancy command-line output
# ----------

def err(msg)
    puts "[ERROR] #{msg}"
end

def warn(msg)
    puts "[WARN] #{msg}"
end

def verbose(msg)
    puts "verbose> #{msg}"
end

# ----------
# Global variables
# ----------

targets_to_build = Targets.keys
flags = Array.new

# ----------
# Command-line handling
# ----------

Flags = {
    "t" => {
        "requires_data" => true,
        "handle" => lambda do |arg|
            targets_to_build = Array.new
            arg.split(",").each { |target| 
                next if target == ""
                if not Targets.keys.include? target
                    warn "Unknown target \"#{target}\""
                    next
                end
                targets_to_build.push target
            }
            verbose "Targets reading is done. To build: #{targets_to_build}"
        end
    }
}

# Command line usage: compile.rb [-t <target>[,][<target>]]
# When called with no arguments, this compiles all targets.

ARGV.each_index { |i|
    if ARGV[i][0] == '-'
        if not Flags.keys.include? ARGV[i][1..-1]
            warn "Unknown command flag #{ARGV[i]}"
            next
        else
            verbose "I see flag #{ARGV[i]}"
            if Flags[ARGV[i][1..-1]]["requires_data"]
                if i + 1 == ARGV.length  
                    err "Flag #{ARGV[i]} requires an argument"
                    return
                end
                Flags[ARGV[i][1..-1]]["handle"].call(ARGV[i+1])
            else
                Flags[ARGV[i][1..-1]]["handle"].call
            end
        end
    end
}

targets_to_build.each do |target|
    puts "--> Building target \"#{target}\""
    Targets[target].each do |cmd|
        puts "---> Running command #{cmd}"
        system cmd
    end
    puts "--> Target \"#{target}\" build done"
end
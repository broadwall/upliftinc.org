/*global $*/
let UpliftJS = (() => {
    
    $("mh1")
        .addClass("text-primary-color")
        .addClass("uk-h1");

    // J-Expand Module
    const JExpandData = {
        "Nav.Links": [
            "Home",
            "About Us",
            "Automation Workz",
            "Activities",
            "Corporate",
            "Invention Revolution",
            "Skilled Trades",
            "Become a Donor",
            "Think Tank"
        ]
    };

    const ExpandTemplates = {
        "ue-donate": 
`<u-section-wrapper bg="darkred">
    <u-section style="display:flex">
        <div>
            <div class="uplift-subtitle">
                Join the Tech Revolution with Us
            </div>
            <u-semilarge>
                Come play with us. Become a partner. Become a donor.
            </u-semilarge>
        </div>
        <u-action class="uplift-pseudolink" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=48SHGPF24FE3Y">
            Click This
        </u-action>
    </u-section>
</u-section-wrapper>` // FIXME(link): "Click This" PayPal link is not authorized.

    };
    
    for (let template in ExpandTemplates) {
        $(template).html(ExpandTemplates[template]);
    }
    
    // Add colors to custom CSS classes
    const ClassColors = {
        "uplift-quote": "dark-primary-color"
    };

    $(".j_expand").each((index, element) => {
        let jqElem = $(element);
        if (jqElem.attr("j-data") !== undefined &&
            jqElem.attr("j-type") !== undefined) {

            let jData = jqElem.attr("j-data");
            let jType = jqElem.attr("j-type");

            if (jData in JExpandData) {
                for (let innerText of JExpandData[jData]) {
                    let innerElem = document.createElement(jType);
                    innerElem.innerHTML = innerText;
                    jqElem.append(innerElem);
                }
            } else {
                console.warn("j-data attribute invalid on .j_expand element");
            }
        } else {
            console.warn("Missing some attributes on .j_expand element");
        }
    });

    for (let key in ClassColors) {
        $(document.getElementsByClassName(key)).addClass(ClassColors[key]);
    }
    
    const SectionBackgroundPresets = {
        "primary": "#000"
    };
   
    // Common Background CSS generation logic
    let generateCssBg = ($elems, $bgSource, applyToChild) => {
        $elems.each((index, elem) => {
            let $elem = $(elem);
            let $bgAttr = 
                ($bgSource === undefined ? $elem : $bgSource).attr("bg");
            if ($bgAttr !== undefined) {
                (applyToChild ? $elem.children() : $elem)
                    .css("background-color", (
                        ($bgAttr in SectionBackgroundPresets) ?
                            SectionBackgroundPresets[$bgAttr] :
                            $bgAttr));
            }
        });  
    };
    
    // uplift-section background CSS generation
    generateCssBg($("u-section-wrapper"));
    
    // uplift-hboxes `background` CSS attribute generation
    generateCssBg($(".uplift-hboxes"), undefined, true);
    
    // PseudoLink onclick
    $(".uplift-pseudolink").each((index, element) => {
        let target = element.getAttribute("href");
        element.onclick = () => {
            if (element.getAttribute("newtab") == "") {
                window.open(target);
            } else {
                window.location.href = target;
            }
        };
    });
    
    // Command line exposed interface
    return {
        moo: () => {
            console.error("This API does not have Super Cow Powers, " +
                "but it does have SuperBull Powers.");
        }
    };
})();
| Date        | Time          | Description         |
|-------------|---------------|---------------------|
| November 19 | 17:00 - 18:00 | Start index.html    |
| November 20 | 20:30 - 21:30 | Reorganize & CSS    |
| November 23 | 15:30 - 16:30 | Reorganize Sections |
| November 24 | 16:30 - 18:00 | Continue framework  |
| November 24 | 20:40 - 21:25 | Compile script      |
| November 25 | 21:15 - 22:00 | Start THRIVING      |
| November 26 | 20:35 - 22:35 | Homepage almost     |
| November 27 | 19:15 - 21:15 | Homepage done       |